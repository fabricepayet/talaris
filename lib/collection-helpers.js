import Shops from '../imports/api/shops/shops-collection';

Meteor.users.helpers({
  getShop () {
    return Shops.findOne({ ownerId: this._id });
  },
  shopName () {
    const shop = Shops.findOne({ ownerId: this._id });
    if (!shop) return null;
    return shop.name;
  },
  shopCurrentPack () {
    const shop = Shops.findOne({ ownerId: this._id });
    if (!shop) return null;
    return shop.currentPack;
  },
});

Shops.helpers({
  owner () {
    return Meteor.users.findOne(this.ownerId);
  },

  getEmail () {
    const user = Meteor.users.findOne(this.ownerId);
    return user.emails[0].address;
  },
});
