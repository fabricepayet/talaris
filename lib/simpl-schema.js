import SimpleSchema from 'simpl-schema';

SimpleSchema.setDefaultMessages({
  initialLanguage: 'fr',
  messages: {
    en: {
      "too_long": "Too long!",
    },

    fr: {
      "required": "{{label}} est requis"
    }
  },
});