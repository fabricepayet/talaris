import './stat-chart.html';

import moment from 'moment';

import Stats from '../../../api/stats/stats-collection';

const lineChartOptions = {
  legend: {
    display: false,
  },
  scales: {
    yAxes: [
      {
        display: false,
        ticks: {
          beginAtZero: true,
          min: 0,
        },
        gridLines: {
          display: false,
        },
      },
    ],
    xAxes: [
      {
        display: false,
        gridLines: {
          display: false,
        },
      },
    ],
  },
};

Template.Stat_Chart.onRendered(function() {
  const stats = Stats.find({}, { $sort: { createdAt: -1 }, limit: 7 }).fetch();

  const statsSorted = _.sortBy(stats, 'createdAt');

  var ctx = Template.instance()
    .find('.statChart')
    .getContext('2d');

  const daysCountData = statsSorted.map(
    stat => stat[this.data.modifier].dayCount,
  );

  lineChartOptions.scales.yAxes[0].ticks.max = _.max(daysCountData) + 3;
  var myLineChart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: statsSorted.map(stat =>
        moment(stat.createdAt).format('DD/MM/YY'),
      ),
      datasets: [
        {
          data: daysCountData,
          label: this.data.label,
          borderColor: 'rgba(34, 34, 95, 0.6)',
          fill: true,
          backgroundColor: 'rgba(34, 34, 95, 0.2)',
        },
      ],
    },
    options: lineChartOptions,
  });
});

Template.Stat_Chart.helpers({
  percentProgress() {
    const lastStat = Stats.findOne({}, { sort: { createdAt: -1 } });
    if (!lastStat) return;
    newWeekCount = lastStat[this.modifier].weekCount;
    const firstStat = Stats.findOne({}, { sort: { createdAt: 1 } });
    if (!firstStat) return;
    previousWeekCount = firstStat[this.modifier].weekCount;
    if (previousWeekCount === 0) {
      return 100;
    }
    const diff = newWeekCount - previousWeekCount;
    return Math.round(diff / previousWeekCount * 100);
  },

  weekCount() {
    const lastStat = Stats.findOne({}, { sort: { createdAt: 1 } });
    if (!lastStat) return;
    return lastStat[this.modifier].weekCount;
  },
});

Template.Stat_Chart.onCreated(function() {
  handleStats = this.subscribe('stats');
});
