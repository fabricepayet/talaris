import './communication-form.html';

Template.CommunicationFormComponent.onCreated(function() {
  this.currentMessage = new ReactiveVar('');
});

Template.CommunicationFormComponent.helpers({
  remainingChars() {
    tpl = Template.instance();
    if (!tpl.currentMessage.get()) return;
    let nbChars = tpl.currentMessage.get().length;
    return tpl.data.maxSize - nbChars;
  },

  isDisabled() {
    return Template.instance().currentMessage.get() === '';
  },
});

Template.CommunicationFormComponent.events({
  'keyup textarea'(event, instance) {
    event.preventDefault();
    let message = instance.$('textarea').val();
    Template.instance().currentMessage.set(message);
  },
});
