import './campaign-item.html';

import moment from 'moment';

import { Session } from 'meteor/session';

Template.Campaign_Item.events({
  'click .js-edit-campaign'(event, instance) {
    Session.set('editCampaign', this.campaign);
  },

  'click .js-campaign-delete'(event, instance) {
    bootbox.confirm({
      message: 'Etes vous sûr de vouloir supprimer cette campagne ?',
      buttons: {
        confirm: {
          label: 'Supprimer',
          className: 'btn-danger',
        },
        cancel: {
          label: 'Annuler',
          className: 'btn-light',
        },
      },
      callback: result => {
        if (result) {
          Meteor.call(
            'deleteCampaign',
            { campaignId: this.campaign._id },
            err => {
              toastr.info('La campagne a été supprimée');
            },
          );
        }
      },
    });
  },
});
