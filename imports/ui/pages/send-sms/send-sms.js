import './send-sms.html';

Template.Send_Sms_Page.events({
  'click .js-send'(event, instance) {
    const message = instance.$('textarea').val();
    Meteor.call('sendSmsMethod', { message }, function(err) {
      console.log('====================================');
      console.log('execution error', err);
      console.log('====================================');
    });
  },
});
