import './contact-detail.html';

import Contacts from '../../../api/contacts/contacts-collection';
import Checkouts from '../../../api/checkouts/checkouts-collection';
import Sms from '../../../api/sms/sms-collection';
import Lists from '../../../api/lists/lists-collection';

Template.Contact_Detail_Page.onCreated(function() {
  this.subscribe('lists');
  let handle = this.subscribe(
    'contactDetail',
    FlowRouter.getParam('contactId'),
  );
  this.isReady = new ReactiveVar(false);
  this.autorun(() => {
    this.isReady.set(handle.ready());
  });
});

Template.Contact_Detail_Page.helpers({
  breadcrumbData() {
    return {
      parents: [
        {
          routeName: 'contacts',
          friendlyName: 'Contacts',
        },
      ],
      current: {
        routeName: 'contactDetail',
        friendlyName: 'détails',
      },
    };
  },
  isReady() {
    return Template.instance().isReady.get();
  },

  contact() {
    return Contacts.findOne(FlowRouter.getParam('contactId'));
  },

  lastCheckouts() {
    return Checkouts.find(
      { contactId: FlowRouter.getParam('contactId') },
      { limit: 10, sort: { date: -1 } },
    );
  },

  lastSms() {
    return Sms.find(
      { contactId: FlowRouter.getParam('contactId') },
      { limit: 10, sort: { createdAt: -1 } },
    );
  },

  complementaryFields() {
    return this.birthdayDate && this.gender;
  },

  allLists() {
    return Lists.find();
  },

  isInList() {
    const contact = Contacts.findOne(FlowRouter.getParam('contactId'));
    if (!contact || !contact.lists) {
      return false;
    }
    return contact.lists.indexOf(this._id) !== -1;
  },
});

Template.Contact_Detail_Page.events({
  'change .js-change-list'(event, instance) {
    const listsChecked = [];
    const checkbox = instance
      .$('.contact-detail__lists input[type="checkbox"]:checked')
      .each(function() {
        listsChecked.push($(this).attr('data-list'));
      });

    Meteor.call(
      'editListContact',
      { lists: listsChecked, _id: FlowRouter.getParam('contactId') },
      function(err) {
        if (err) {
          toastr.error(err.message);
        } else {
          toastr.success('Les listes ont été modifiées');
        }
      },
    );
  },

  'click .js-delete-contact'(event, instance) {
    bootbox.confirm({
      message: 'Êtes-vous sûr de vouloir supprimer ce contact ?',
      buttons: {
        confirm: {
          label: 'Supprimer',
          className: 'btn-danger',
        },
        cancel: {
          label: 'Annuler',
          className: 'btn-light',
        },
      },
      callback: result => {
        if (result) {
          Meteor.call(
            'deleteContactMethod',
            { contactId: FlowRouter.getParam('contactId') },
            err => {
              if (err) {
                toastr.error(err.message);
              } else {
                toastr.success('Le contact a été supprimé');
                FlowRouter.go('contacts');
              }
            },
          );
        }
      },
    });
  },
});

Template.Edit_Birthday_Modal.onRendered(function() {
  var picker = new Pikaday({
    field: Template.instance().find('#datepicker'),
    format: 'DD/MM/YYYY',
  });
});

Template.Edit_Note_Modal.events({
  'click .js-edit-info-contact'(event, instance) {
    const note = instance.$('textarea').val();
    if (!note) { 
      toastr.error('Vous ne pouvez pas ajouter une note vide')
      return
    };
    Meteor.call(
      'editContactInfoMethod',
      { note, _id: FlowRouter.getParam('contactId') },
      function(err) {
        if (err) {
          toastr.error(err.message);
        } else {
          instance.$('.edit-note-modal').modal('hide');
          toastr.success('Le contact a bien été modifié');
        }
      },
    );
  },
});

Template.Edit_Birthday_Modal.events({
  'click .js-edit-info-contact'(event, instance) {
    const birthdayDate = instance.$('input[name="birthdayDate"]').val();
    Meteor.call(
      'editContactInfoMethod',
      { birthdayDate, _id: FlowRouter.getParam('contactId') },
      function(err) {
        if (err) {
          toastr.error(err.message);
        } else {
          instance.$('.edit-birthday-modal').modal('hide');
          toastr.success('Le contact a bien été modifié');
        }
      },
    );
  },
});

Template.Edit_Gender_Modal.events({
  'click .js-edit-info-contact'(event, instance) {
    const gender = instance.$('select[name="gender"]').val();
    Meteor.call(
      'editContactInfoMethod',
      { gender, _id: FlowRouter.getParam('contactId') },
      function(err) {
        if (err) {
          toastr.error(err.message);
        } else {
          instance.$('.edit-gender-modal').modal('hide');
          toastr.success('Le contact a bien été modifié');
        }
      },
    );
  },
});
