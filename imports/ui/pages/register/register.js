import './register.html';

import { Accounts } from 'meteor/accounts-base';

Template.Register_Page.onCreated(function() {
  this.isLoading = new ReactiveVar(false);
  this.errorMessage = new ReactiveVar(null);
});

Template.Register_Page.helpers({
  isLoading() {
    return Template.instance().isLoading.get();
  },

  errorMessage() {
    return Template.instance().errorMessage.get();
  },
});

Template.Register_Page.events({
  'submit #registerForm'(event, instance) {
    event.preventDefault();
    const $emailField = instance.$('#inputEmail');
    const $passwordField = instance.$('#inputPassword');
    const $shopField = instance.$('#inputShop');

    instance.isLoading.set(true);
    Accounts.createUser(
      {
        email: $emailField.val(),
        password: $passwordField.val(),
        shopName: $shopField.val(),
      },
      function(err) {
        instance.isLoading.set(false);
        if (err) {
          if (err.error === 499) {
            toastr.info(
              'Un email de validation a été envoyé, veuillez vérifier votre boite email.',
            );
            FlowRouter.go('/login');
            return;
          }

          toastr.error(err.message, 'Account creation error');
        } else {
          toastr.success('Votre compte a été créé');
        }
      },
    );
  },
});
