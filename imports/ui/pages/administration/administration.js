import { Session } from 'meteor/session';

import './administration.html';
import Shops from '../../../api/shops/shops-collection';

Template.Administration_Page.events({
  'submit #enrollUserForm'(event, instance) {
    event.preventDefault();
    const shopName = instance.$('#inputShop').val();
    const email = instance.$('#inputEmail').val();
    if (!shopName || !email) return;
    Meteor.call('enrollUser', { shopName, email });
    instance.$('#inputShop').val('');
    instance.$('#inputEmail').val('');
    toastr.success("L'utilisateur a été créé. Un email lui a été envoyé.");
    instance.$('.add-user-modal').modal('hide');
  },
});

Template.Administration_Page.helpers({
  UserSchema() {
    return Meteor.user();
  },

  currentShop() {
    return Session.get('currentAdminEditShop');
  },
});

Template.adminEditContactIcon.events({
  'click .js-edit-shop-admin-modal'(event, instance) {
    const shop = Shops.findOne({ ownerId: this._id });
    Session.set('currentAdminEditShop', shop);
  },
});

Template.adminEditContactIcon.helpers({
  isUserAdmin() {
    return Roles.userIsInRole(this._id, ['admin']);
  },
});

Template.editShopAdmin.events({
  'click .js-save-shop-admin'(event, instance) {
    event.preventDefault();
    const remainingCredit = instance.$('#remainingCredit').val();
    const senderName = instance.$('#senderName').val();

    if (!remainingCredit || !senderName) return;

    const currentEditShop = Session.get('currentAdminEditShop');
    if (!currentEditShop) return;

    Meteor.call(
      'editShopAdmin',
      {
        senderName,
        remainingCredit: parseInt(remainingCredit),
        shopId: currentEditShop._id,
      },
      err => {
        instance.$('.edit-admin-shop-modal').modal('hide');
        if (err) {
          toastr.error(err.reason);
          return;
        }
        toastr.success('La boutique a été modifiée');
      },
    );
  },

  'hidden.bs.modal .edit-admin-shop-modal'(event, instance) {
    instance.$('#editShopAdminForm')[0].reset();
    Session.set('currentAdminEditShop', null);
  },
});
