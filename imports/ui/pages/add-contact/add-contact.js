import './add-contact.html';
import { ContactSchema } from '../../../api/contacts/contacts-collection';

const hooksObject = {
  onError: function(formType, error) {
    if (error && error.reason) {
      toastr.error(error.reason);
    }
  },

  before: {
    method: function(doc) {
      if (FlowRouter.getParam('listId')) {
        doc.lists = [FlowRouter.getParam('listId')];
      } else {
        doc.lists = [];
      }
      if (doc.phone) {
        if (doc.phone.startsWith('069')) {
          doc.phone = '+262' + doc.phone.substring(1);
        }
      }
      return doc;
    },
  },

  onSuccess: function() {
    mixpanel.track('contact_added')
    $('.add-contact-modal').modal('toggle');
    toastr.success('Nouveau contact ajouté');
  },
};

AutoForm.addHooks('addContact', hooksObject, true);

Template.Add_Contact_Modal.helpers({
  ContactSchema() {
    return ContactSchema.omit('shopId').omit('_id');
  },
});

Template.Add_Contact_Modal.events({
  'hidden.bs.modal .add-contact-modal'(event, instance) {
    instance.$('#addContact')[0].reset();
  },
});
