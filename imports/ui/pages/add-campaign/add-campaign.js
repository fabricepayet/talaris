            import './add-campaign.html';

import { CampaignSchema } from '../../../api/campaigns/campaigns-collection';
import Lists from '../../../api/lists/lists-collection';

const hooksObject = {
  onError: function(formType, error) {
    toastr.error(error.reason);
  },
  before: {
    method: function(doc) {
      doc.shopId = Meteor.user().getShop()._id;
      if (!doc.lists) {
        doc.lists = [];
      }
      return doc;
    },
  },
  onSuccess: function() {
    mixpanel.track('campaign_added')
    $('.add-campaign-modal').modal('toggle');
    $('#addCampaign')[0].reset();
    toastr.success('La campagne a bien été ajouté');
  },
};

AutoForm.addHooks('addCampaign', hooksObject, true);

Template.Add_Campaign_Modal.onCreated(function() {
  this.subscribe('lists');
  this.currentMessage = new ReactiveVar('');
  this.makeReactiveHack = new ReactiveVar(null);
  this.allContactSelected = new ReactiveVar(true);
  this.listsSelected = new ReactiveVar(false);
});

Template.Add_Campaign_Modal.helpers({
  CampaignSchema() {
    return CampaignSchema.omit('state');
  },

  campaignType() {
    Template.instance().makeReactiveHack.get();
    return AutoForm.getFieldValue('type', 'addCampaign');
  },

  remainingChars() {
    let nbChars = Template.instance().currentMessage.get().length;
    return 160 - nbChars;
  },

  allContactSelected() {
    return Template.instance().allContactSelected.get();
  },

  invalid() {
    return (
      !Template.instance().listsSelected.get() &&
      !Template.instance().allContactSelected.get()
    );
  },

  hasLists() {
    return Lists.find().count();
  },
});

Template.Add_Campaign_Modal.onRendered(function() {
  Template.instance().$('.popover-help').popover({
    trigger: 'hover'
  })
})

Template.Add_Campaign_Modal.events({
  'click .js-close-modal'(event, instance) {
    instance.$('#addCampaign')[0].reset();
    instance.$('.add-campaign-modal').modal('toggle');
  },

  'keyup textarea'(event, instance) {
    event.preventDefault();
    let message = instance.$('textarea').val();
    Template.instance().currentMessage.set(message);
  },

  'hidden.bs.modal .add-campaign-modal'(event, instance) {
    instance.$('#addCampaign')[0].reset();
  },

  'shown.bs.modal .add-campaign-modal'(event, instance) {
    instance.allContactSelected.set(true);
    instance.listsSelected.set(false);
    instance.$('input[name="lists"]').attr('checked', false);
    instance.makeReactiveHack.set('hack');
  },

  'click input[name="type"]'(event, instance) {
    instance.makeReactiveHack.set(instance.$(event.currentTarget).val());
  },

  'change #allContactsCheckbox'(event, instance) {
    const isChecked = instance.allContactSelected.get();
    instance.allContactSelected.set(!isChecked);
    if (instance.$('#allContactsCheckbox').is(':checked')) {
      instance.$('input[name="lists"]').attr('checked', false);
    } else {
      instance.$('input[name="lists"]').attr('disabled', false);
    }
  },

  'change input[name="lists"]'(event, instance) {
    const islistSelected = instance.$('input[name="lists"]:checked').length > 0;
    instance.listsSelected.set(islistSelected);
    if (islistSelected) {
      instance.allContactSelected.set(false);
      instance.$('#allContactsCheckbox').attr('checked', false);
    }
  },
});
