import './login.html';

import { Session } from 'meteor/session';

Template.Login_Page.onCreated(function() {
  this.errorMessage = new ReactiveVar(null);
});

Template.Login_Page.events({
  'submit #loginForm'(event, instance) {
    Session.set('emailUnverified', null);
    Session.set('forgotPassword', null);
    event.preventDefault();
    instance.errorMessage.set(null);
    const emailField = instance.$('#inputEmail');
    const passwordField = instance.$('#inputPassword');
    Meteor.loginWithPassword(emailField.val(), passwordField.val(), function(
      err,
    ) {
      if (err) {
        if (err.error === 499) {
          Session.set('emailUnverified', emailField.val());
          return;
        }

        if (err.error === 403) {
          instance.errorMessage.set('Identifiant ou mot de passe non valide.');
          Session.set('forgotPassword', emailField.val());
          return;
        }
        toastr.error(err.message, 'Login failed');
      } else {
        FlowRouter.go('dashboard');
      }
    });
  },

  'click .js-send-validation-email'(event, instance) {
    const userEmail = Session.get('emailUnverified');
    Session.set('emailUnverified', null);
    Meteor.call('sendVerificationEmail', { userEmail }, err => {
      if (err) {
        toastr.error(err.message);
        return;
      }
      toastr.info('Un email de validation a été envoyé.');
    });
  },

  'click .js-forgot-password'(event, instance) {
    const email = Session.get('forgotPassword');
    Session.set('forgotPassword', null);
    Accounts.forgotPassword({ email }, err => {
      // if (err) {
      //   toastr.error(err.message);
      // } else {
      // }
      toastr.info(`Un email a été envoyé à ${email}`);
    });
  },
});

Template.Login_Page.helpers({
  errorMessage() {
    return Template.instance().errorMessage.get();
  },

  emailUnverified() {
    return Session.get('emailUnverified');
  },

  forgotPassword() {
    return Session.get('forgotPassword');
  },
});
