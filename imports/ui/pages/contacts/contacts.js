import './contacts.html';

import Contacts from '../../../api/contacts/contacts-collection';

Template.Contacts_Page.onCreated(function() {
  this.subscribe('countContacts');
});

Template.Contacts_Page.helpers({
  breadcrumdData() {
    return {
      current: {
        friendlyName: 'Contacts',
      },
    };
  },

  contactCount() {
    return Counter.get('nbContacts');
  },
});

Template.Contacts_Page.events({
  'click tbody > tr'(event) {
    var dataTable = $(event.target)
      .closest('table')
      .DataTable();
    var rowData = dataTable.row(event.currentTarget).data();
    if (!rowData) return; // Won't be data if a placeholder row is clicked

    FlowRouter.go('contactDetail', { contactId: rowData._id });
  },
});
