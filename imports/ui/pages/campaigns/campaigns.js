import './campaigns.html';

import Campaigns from '../../../api/campaigns/campaigns-collection';

Template.Campaigns_Page.onCreated(function() {
  this.subscribe('campaigns');
});

Template.Campaigns_Page.helpers({
  campaigns() {
    return Campaigns.find(
      {},
      {
        sort: {
          type: -1,
        },
      },
    );
  },

  breadcrumdData() {
    return {
      current: {
        friendlyName: 'Campagnes',
      },
    };
  },
});
