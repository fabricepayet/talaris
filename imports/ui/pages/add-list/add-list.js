import './add-list.html';

import { ListSchema } from '../../../api/lists/lists-collection';

const hooksObject = {
  onError: function(formType, error) {
    toastr.error(error.reason);
  },

  before: {
    method: function(doc) {
      return doc;
    },
  },

  onSuccess: function() {
    $('.add-list-modal').modal('hide');
    toastr.success('La liste a été créée');
  },
};

AutoForm.addHooks('addList', hooksObject, true);

Template.Add_List_Modal.onCreated(function() {});

Template.Add_List_Modal.helpers({
  ListSchema() {
    return ListSchema;
  },
});

Template.Add_List_Modal.events({
  'hidden.bs.modal .add-list-modal'(event, instance) {
    instance.$('#addList')[0].reset();
  },
});
