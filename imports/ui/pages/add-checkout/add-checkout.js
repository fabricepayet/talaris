import './add-checkout.html';

Template.Add_Checkout_Modal.events({
  'click .js-add-list'(event, instance) {
    const contactId = FlowRouter.getParam('contactId');
    const description = instance.$('textarea').val();
    Meteor.call('addCheckout', { description, contactId }, function(err) {
      if (err) {
        toastr.error(err.message);
      } else {
        toastr.success('Le passage a été enregistré');
        instance.$('textarea').val('');
        instance.$('.modal').modal('toggle');
      }
    });
  },
});
