import './update-shop.html';

import { ShopSchema } from '../../../api/shops/shops-collection';

const hooksObject = {
  onSuccess: function(formType, result) {
    FlowRouter.go('dashboard');
  },

  onError: function(formType, error) {
    toastr.error(error.reason, 'Update error');
  },

  onSubmit: function(insertDoc, updateDoc, currentDoc) {
    Meteor.call('updateShopMethod', { id: currentDoc._id, shop: insertDoc });
    this.done();
  },
};

AutoForm.addHooks('updateShop', hooksObject, true);

Template.Update_Shop_Page.helpers({
  userShop() {
    return Meteor.user().getShop();
  },

  ShopSchema() {
    return ShopSchema;
  },
});
