import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';

SimpleSchema.extendOptions(['autoform']);

const Lists = new Mongo.Collection('lists');

Lists.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

// Define the schema
export const ListSchema = new SimpleSchema(
  {
    name: {
      type: String,
      label: 'Nom',
    },
    count: {
      type: Number,
      defaultValue: 0,
      optional: true,
      autoform: {
        omit: true,
      },
    },
    shopId: {
      type: String,
      label: 'Shop',
      optional: true,
      autoValue() {
        let shop = Meteor.user().getShop();
        return shop._id;
      },
      autoform: {
        omit: true,
      },
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      optional: true,
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      optional: true,
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Lists.attachSchema(ListSchema);

export default Lists;
