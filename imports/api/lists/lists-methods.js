import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import Shops from '../shops/shops-collection';
import Lists, { ListSchema } from './lists-collection';

const addListMethod = new ValidatedMethod({
  name: 'addListMethod',
  validate: ListSchema.validator(),
  run(list) {
    const shop = Meteor.user().getShop();
    list.shopId = shop._id;
    Lists.insert(list);
  },
});

submitEditListSchema = new SimpleSchema({
  _id: {
    type: String,
  },
  name: {
    type: String,
  },
});

const deleteListMethod = new ValidatedMethod({
  name: 'deleteListMethod',
  validate: new SimpleSchema({
    listId: String,
  }).validator(),
  run({ listId }) {
    const shop = Meteor.user().getShop();
    const originalList = Lists.findOne(listId);
    if (!originalList) {
      throw new Error('List should exists');
    }
    if (originalList.shopId != shop._id) {
      throw new Error('Should be the owner of the list');
    }
    Lists.remove(listId);
  },
});

const editListMethod = new ValidatedMethod({
  name: 'editListMethod',
  validate: new SimpleSchema({
    list: { type: submitEditListSchema },
  }).validator(),
  run({ list }) {
    const shop = Meteor.user().getShop();
    const originalList = Lists.findOne(list._id);
    if (!originalList) {
      throw new Error('List should exists');
    }
    if (originalList.shopId != shop._id) {
      throw new Error('Should be the owner of the list');
    }
    Lists.update(list._id, {
      $set: {
        name: list.name,
      },
    });
  },
});
