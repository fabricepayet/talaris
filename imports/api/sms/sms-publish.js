import moment from 'moment';

import Sms from './sms-collection';
import Shops from '../shops/shops-collection';

Meteor.publish('countWeekSms', function() {
  if (!this.userId) {
    return;
  }
  const shop = Shops.findOne({
    ownerId: this.userId,
  });
  const beginWeekDate = moment()
    .startOf('week')
    .toISOString();
  return new Counter(
    'nbWeekSms',
    Sms.find({
      shopId: shop._id,
      createdAt: { $gte: new Date(beginWeekDate) },
    }),
  );
});

Meteor.publish('lastSms', function(contactId) {
  if (!this.userId) {
    return;
  }
  return Sms.find({ contactId }, { sort: { createdAt: -1 }, limit: 5 });
});
