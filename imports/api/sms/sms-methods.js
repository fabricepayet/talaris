import twilio from 'twilio';
import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import TwilioApi from '../twilio/twilio-api';

const { accountSid, authToken } = Meteor.settings.twilio;

var client = new twilio(accountSid, authToken);

// to remove or decrement credits
const sendSmsMethod = new ValidatedMethod({
  name: 'sendSmsMethod',
  validate: new SimpleSchema({
    message: String,
    to: String,
  }).validator(),
  run({ message, to }) {
    const shop = Meteor.user().getShop();
    if (!shop) return;
    TwilioApi.sendMessage(message, shop, to);
  },
});
