import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';

const sendVerificationEmail = new ValidatedMethod({
  name: 'sendVerificationEmail',
  validate() {
    userEmail: String;
  },
  run({ userEmail }) {
    const user = Meteor.users.findOne({ 'emails.address': userEmail });
    if (!user) {
      throw new Error('Unknow user');
    }
    Accounts.sendVerificationEmail(user._id, userEmail);
  },
});

const enrollUser = new ValidatedMethod({
  name: 'enrollUser',
  validate: new SimpleSchema({
    email: { type: String, regEx: SimpleSchema.RegEx.Email },
    shopName: { type: String },
  }).validator(),
  run({ email, shopName }) {
    const loggedInUser = Meteor.user();

    if (!loggedInUser || !Roles.userIsInRole(loggedInUser, ['admin'])) {
      throw new Meteor.Error(403, 'Access denied');
    }
    var userId = Accounts.createUser({ email, shopName });
    Accounts.sendEnrollmentEmail(userId);
  },
});
