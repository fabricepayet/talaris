import Stats from './stats-collection';

Meteor.publish('stats', function() {
  if (!this.userId) {
    return;
  }

  const shop = Meteor.user().getShop();
  if (!shop) return;
  return Stats.find(
    { shopId: shop._id },
    { limit: 7, sort: { createdAt: -1 } },
  );
});
