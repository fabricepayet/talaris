const { accountSid, authToken } = Meteor.settings.twilio;
import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
const clientPricing = require('twilio')(accountSid, authToken).pricing;
const client = require('twilio')(accountSid, authToken);
const twilio = require('twilio');

import Contacts from '../contacts/contacts-collection';

const TwilioApi = {
  getPricing() {
    clientPricing.phoneNumbers
      .countries('FR')
      .fetch()
      .then(country => {
        country.phoneNumberPrices.forEach(price => {
          console.log(`${price.number_type} ${price.current_price}`);
        });
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  },

  listCountries() {
    let client = require('twilio')(accountSid, authToken).pricing;

    clientPricing.phoneNumbers.countries
      .list()
      .then(countries => {
        countries.forEach(country => {
          console.log(country.isoCountry);
        });
      })
      .catch(error => {
        console.log(error);
        throw error;
      });
  },

  listIncomingPhoneNumber() {
    client.incomingPhoneNumbers.each(({ phoneNumber, sid, friendlyName }) =>
      console.log(phoneNumber, sid, friendlyName),
    );
  },

  updatePhoneNumber(numberSid, friendlyName) {
    client
      .incomingPhoneNumbers(numberSid)
      .update({
        friendlyName,
      })
      .then(number => console.log(number.friendlyName))
      .catch(err => console.error(err));
  },

  createSubAccount(shopName, callback) {
    if (!Meteor.isProduction) {
      shopName = `dev__${shopName}`;
    }
    client.api.accounts
      .create({ friendlyName: shopName })
      .then(account => callback(null, account))
      .catch(err => callback(err));
    // process.stdout.write(account.sid)
  },

  sendMessage(message, shop, contactId) {
    if (
      !shop.twilioConfig ||
      !shop.twilioConfig.authToken ||
      !shop.twilioConfig.sid ||
      !shop.twilioConfig.senderId
    ) {
      throw new Error('Shop not configured to sent SMS');
    }
    const { authToken, sid, senderId } = shop.twilioConfig;
    const twilioShopClient = twilio(sid, authToken);
    const contact = Contacts.findOne(contactId);

    console.log(`Sending an SMS to ${contact.phone} from ${senderId}`);
    if (Meteor.isProduction) {
      twilioShopClient.messages
        .create({
          body: message,
          to: contact.phone,
          from: senderId,
        })
        .then(response => console.log('Le SMS a été envoyé!'))
        .catch(e => console.error(e));
    } else {
      console.log("[DEV MODE] le message n'a pas été envoyé.");
    }
  },

  findAndBuyPhoneNumber(friendlyName, callback) {
    client
      .availablePhoneNumbers('US')
      .local.list({
        smsEnabled: 'true',
      })
      .then(data => {
        if (Meteor.isProduction) {
          client.incomingPhoneNumbers
            .create({
              phoneNumber: data[0].phoneNumber,
              friendlyName,
            })
            .then(purchasedNumber => {
              console.info(
                `Buying a number for ${friendlyName}. Number found ${
                  purchasedNumber.phoneNumber
                }`,
              );
              callback(null, purchasedNumber);
            })
            .catch(err => callback(err));
        } else {
          console.info('DEV ENVIRONMENT: not buying a new number');
          callback(null, data[0]);
        }
      })
      .catch(err => callback(err));
  },
};

//   findAndBuyPhoneNumber(friendlyName, callback) {
//     client
//       .availablePhoneNumbers('US')
//       .local.list({
//         smsEnabled: 'true',
//       })
//       .then(data => {
//         console.log('list ok', data);
//         client.incomingPhoneNumbers
//           .create({
//             phoneNumber: data[0].phoneNumber,
//             friendlyName,
//           })
//           .then(purchasedNumber => {
//             console.info(
//               `Buying a number for ${friendlyName}. Number found ${
//                 purchasedNumber.phoneNumber
//               }`,
//             );
//             callback(err, purchasedNumber);
//           })
//           .catch(err => callback(err));
//       })
//       .catch(err => callback(err));
//   },
// };

const getTwilioPricing = new ValidatedMethod({
  name: 'getTwilioPricing',
  validate() {
    // return true;
  },
  run() {
    TwilioApi.listIncomingPhoneNumber();
  },
});

export default TwilioApi;
