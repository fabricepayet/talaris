import Contacts from './contacts-collection';
import Sms from '../sms/sms-collection';
import Checkouts from '../checkouts/checkouts-collection';
import Shops from '../shops/shops-collection';
import moment from 'moment';
import { check } from 'meteor/check'

Meteor.publish('countContacts', function(selector) {
  if (!this.userId) {
    return;
  }
  const shop = Shops.findOne({ ownerId: this.userId });
  if (!shop) return;
  return new Counter(
    'nbContacts',
    Contacts.find({
      shopId: shop._id,
    }),
  );
});

publishComposite('contactDetail', function(contactId) {
  return {
    find() {
      check(contactId, String)
      if (!this.userId) {
        return;
      }
      return Contacts.find({ _id: contactId });
    },
    children: [
      {
        find(contact) {
          return Sms.find(
            { contactId: contact._id },
            { limit: 5, sort: { createdAt: -1 } },
          );
        },
      },
      {
        find(contact) {
          return Checkouts.find(
            { contactId: contact._id },
            { limit: 5, sort: { createdAt: -1 } },
          );
        },
      },
    ],
  };
});
