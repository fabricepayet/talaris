import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import Contacts from './contacts-collection';
import Lists from '../lists/lists-collection';
import Shops from '../shops/shops-collection';

submitContactSchema = new SimpleSchema({
  firstname: {
    type: String,
    label: 'Prénom',
  },
  lastname: {
    type: String,
    label: 'Nom',
  },
  email: {
    type: String,
    label: 'Email',
    optional: true,
    regEx: SimpleSchema.RegEx.Email,
  },
  phone: {
    type: String,
    label: 'Téléphone',
  },
  zipcode: {
    type: String,
    label: 'Code Postal',
  },
  sexe: {
    type: String,
    allowedValues: ['men', 'women'],
    label: 'Sexe',
  },
});

const publicAddContactInList = new ValidatedMethod({
  name: 'publicAddContactInList',
  validate: new SimpleSchema({
    contact: { type: submitContactSchema },
    shop: { type: String },
  }).validator(),
  run({ shop, contact }) {
    const shopRelated = Shops.findOne({ slug: shop });
    contact.shopId = shopRelated._id;
    contact.channel = 'website';
    Contacts.insert(contact);
  },
});

const editContactMethod = new ValidatedMethod({
  name: 'editContactMethod',
  validate: new SimpleSchema({
    _id: {
      type: String,
    },
    firstname: {
      type: String,
      label: 'Prénom',
      optional: true,
    },
    lastname: {
      type: String,
      label: 'Nom',
    },
    email: {
      type: String,
      label: 'Email',
      regEx: SimpleSchema.RegEx.Email,
      optional: true,
    },
    phone: {
      type: String,
      label: 'Téléphone',
      optional: true,
    },
    zipcode: {
      type: String,
      label: 'Code Postal',
      optional: true,
    },
  }).validator(),
  run(contact) {
    let shop = Meteor.user().getShop();
    if (!shop) {
      throw new Error('No shop for the current user');
    }
    const originalContact = Contacts.findOne(contact._id);
    if (!originalContact) {
      throw new Error('Contact not found');
    }
    if (originalContact.shopId !== shop._id) {
      throw new Error('Should be the owner of the contact');
    }
    $set = {};
    $unset = {};
    if (contact.firstname) {
      $set.firstname = contact.firstname;
    } else {
      $unset.firstname = '';
    }
    if (contact.lastname) {
      $set.lastname = contact.lastname;
    } else {
      $unset.lastname = '';
    }
    if (contact.email) {
      $set.email = contact.email;
    } else {
      $unset.email = '';
    }
    if (contact.phone) {
      $set.phone = contact.phone;
    } else {
      $unset.phone = '';
    }
    if (contact.zipcode) {
      $set.zipcode = contact.zipcode;
    } else {
      $unset.zipcode = '';
    }

    Contacts.update(contact._id, {
      $set,
      $unset,
    });
  },
});

const editContactInfoMethod = new ValidatedMethod({
  name: 'editContactInfoMethod',
  validate: new SimpleSchema({
    _id: {
      type: String,
    },
    birthdayDate: {
      type: String,
      label: "Date d'anniversaire",
      optional: true,
    },
    gender: {
      type: String,
      label: 'Genre',
      optional: true,
    },
    note: {
      type: String,
      optional: true,
    },
  }).validator(),
  run(contact) {
    let shop = Meteor.user().getShop();
    if (!shop) {
      throw new Error('No shop for the current user');
    }
    const originalContact = Contacts.findOne(contact._id);
    if (!originalContact) {
      throw new Error('Contact not found');
    }
    if (originalContact.shopId !== shop._id) {
      throw new Error('Should be the owner of the contact');
    }

    const updateObject = {};

    if (contact.birthdayDate) {
      dateSplitted = contact.birthdayDate.split('/');
      updateObject.birthdayDate = new Date(
        `${dateSplitted[2]}-${dateSplitted[1]}-${dateSplitted[0]}`,
      );
    }
    if (contact.gender) {
      updateObject.gender = contact.gender;
    }
    if (contact.note) {
      updateObject.note = contact.note;
    }
    Contacts.update(contact._id, {
      $set: updateObject,
    });
  },
});

const editListContact = new ValidatedMethod({
  name: 'editListContact',
  validate: new SimpleSchema({
    _id: String,
    lists: [String],
  }).validator(),
  run({ _id, lists }) {
    let shop = Meteor.user().getShop();
    if (!shop) {
      throw new Error('No shop for the current user');
    }
    const originalContact = Contacts.findOne(_id);
    if (!originalContact) {
      throw new Error('Contact not found');
    }
    if (originalContact.shopId !== shop._id) {
      throw new Error('Should be the owner of the contact');
    }
    lists.forEach(function(listId) {
      const list = Lists.findOne(listId);
      if (list.shopId !== shop._id) {
        throw new Error('Should be the owner of the list');
      }
    });
    Contacts.update(_id, { $set: { lists } });
  },
});

const deleteContactMethod = new ValidatedMethod({
  name: 'deleteContactMethod',
  validate: new SimpleSchema({
    contactId: String,
  }).validator(),
  run({ contactId }) {
    const shop = Meteor.user().getShop();
    const originalContact = Contacts.findOne(contactId);
    if (!originalContact) {
      throw new Error('Contact should exists');
    }
    if (originalContact.shopId != shop._id) {
      throw new Error('Should be the owner of the contact');
    }
    Contacts.remove(contactId);
  },
});

const addContactMethod = new ValidatedMethod({
  name: 'addContactMethod',
  validate: new SimpleSchema({
    firstname: {
      type: String,
      label: 'Prénom',
    },
    lastname: {
      type: String,
      label: 'Nom',
      optional: true,
    },
    email: {
      type: String,
      label: 'Email',
      regEx: SimpleSchema.RegEx.Email,
      optional: true,
    },
    phone: {
      type: String,
      label: 'Téléphone',
      optional: true,
    },
    zipcode: {
      type: String,
      label: 'Code Postal',
      optional: true,
    },
    lists: [String],
  }).validator(),
  run(contact) {
    let shop = Meteor.user().getShop();
    if (!shop) {
      throw new Error('No shop for the current user');
    }
    contact.lists.forEach(function(listId) {
      const list = Lists.findOne(listId);
      if (list.shopId !== shop._id) {
        throw new Error('Should be the owner of the list');
      }
    });
    contact.channel = 'manually';
    contact.shopId = shop._id;
    Contacts.insert(contact);
  },
});
