import SimpleSchema from 'simpl-schema';
import moment from 'moment';

import { Tracker } from 'meteor/tracker';
import Lists from '../lists/lists-collection';
import Shops from '../shops/shops-collection';

SimpleSchema.extendOptions(['autoform']);

const Campaigns = new Mongo.Collection('campaigns');

Campaigns.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

const CountSchema = new SimpleSchema({
  sms: {
    type: Number,
    defaultValue: 0,
    optional: true,
  },
});

const FrequencySchema = new SimpleSchema({
  number: {
    type: Number,
    defaultValue: 1,
  },
  unit: {
    type: String,
    allowedValues: ['once', 'day', 'week', 'month', 'year'],
    defaultValue: 'once',
    autoform: {
      afFieldInput: {
        type: 'select',
        firstOption: false,
        options: function() {
          return [
            {
              label: 'Une seule fois',
              value: 'once',
            },
            // {
            //   label: 'Tous les jours',
            //   value: 'day',
            // },
            // {
            //   label: 'Tous les semaines',
            //   value: 'week',
            // },
            // {
            //   label: 'Tous les mois',
            //   value: 'month',
            // },
            {
              label: 'Tous les ans',
              value: 'year',
            },
          ];
        },
      },
    },
  },
});

const delaySchema = new SimpleSchema({
  // field: {
  //   type: String,
  //   label: 'Champ de référence',
  //   allowedValues: ['createdAt', 'birthdayDate', 'lastCheckoutDate'],
  //   autoform: {
  //     afFieldInput: {
  //       type: 'select',
  //       firstOption: '(Sélectionnez un ...)',
  //       options: function() {
  //         return [
  //           {
  //             label: 'Date de création',
  //             value: 'createdAt',
  //           },
  //           {
  //             label: "Date d'anniversaire",
  //             value: 'birthdayDate',
  //           },
  //           {
  //             label: 'Dernier date de passage',
  //             value: 'lastCheckoutDate',
  //           },
  //         ];
  //       },
  //     },
  //   },
  // },
  period: {
    type: String,
    allowedValues: ['day', 'month', 'week', 'year'],
    label: 'Unité',
    autoform: {
      afFieldInput: {
        type: 'select',
        firstOption: '(Sélectionnez un ...)',
        options: function() {
          return [
            {
              label: 'jour(s)',
              value: 'day',
            },
            {
              label: 'semaine(s)',
              value: 'week',
            },
            {
              label: 'mois',
              value: 'month',
            },
            {
              label: 'an(s)',
              value: 'year',
            },
          ];
        },
      },
    },
  },
  number: {
    type: Number,
    label: 'Nombre',
    autoform: {
      afFieldInput: {
        type: 'select',
        firstOption: '(Sélectionnez un ...)',
        options: function() {
          const options = [];
          for (var i = 1; i <= 30; i++) {
            options.push({
              label: `${i}`,
              value: i,
            });
          }
          return options;
        },
      },
    },
  },
  position: {
    type: String,
    label: 'Position',
    allowedValues: ['after', 'before'],
    autoform: {
      afFieldInput: {
        type: 'select',
        firstOption: '(Sélectionnez un ...)',
        options: function() {
          return [
            {
              label: 'avant',
              value: 'before',
            },
            {
              label: 'aprés',
              value: 'after',
            },
          ];
        },
      },
    },
  },
});

export const CampaignSchema = new SimpleSchema(
  {
    name: {
      type: String,
      label: 'Nom',
    },

    dateRef: {
      type: String,
      optional: true,
      label: 'Champ de référence',
      allowedValues: ['createdAt', 'birthdayDate', 'lastCheckoutDate'],
      autoform: {
        afFieldInput: {
          type: 'select',
          firstOption: '(Sélectionnez un ...)',
          options: function() {
            return [
              {
                label: 'Date de création',
                value: 'createdAt',
              },
              {
                label: "Date d'anniversaire",
                value: 'birthdayDate',
              },
              {
                label: 'Dernier date de passage',
                value: 'lastCheckoutDate',
              },
            ];
          },
        },
      },
    },

    delay: {
      type: delaySchema,
      optional: true,
    },

    executionDate: {
      type: Date,
      optional: true,
      min: new Date(
        moment()
          .add(1, 'days')
          .startOf('day'),
      ),
    },

    frequency: {
      type: FrequencySchema,
    },

    sentCount: { type: CountSchema, optional: true },

    message: {
      type: String,
      label: 'Message',
      max: 160,
    },

    type: {
      type: String,
      optional: true,
      allowedValues: ['automatic', 'planned'],
      defaultValue: 'planned',
      autoform: {
        afFieldInput: {
          type: 'select-radio-inline',
          options: function() {
            return [
              {
                label: 'Date fixe',
                value: 'planned',
              },
              {
                label: 'Automatique',
                value: 'automatic',
              },
            ];
          },
        },
      },
    },

    lists: {
      type: Array,
      label: 'Listes',
      autoform: {
        afFieldInput: {
          type: 'select-checkbox',
          options: function() {
            const shop = Meteor.user().getShop();
            if (!shop) return;
            const lists = Lists.find({ shopId: shop._id }).fetch();
            const listsId = lists.map(list => ({
              value: list._id,
              label: `${list.name} (${list.count} contacts)`,
            }));
            return listsId;
          },
        },
      },
    },
    'lists.$': {
      type: String,
      regEx: SimpleSchema.RegEx.Id,
    },
    shopId: {
      type: String,
      label: 'Shop',
      // defaultValue: Meteor.user().getShop()._id,
      optional: true,
      autoform: {
        omit: true,
      },
    },
    state: {
      type: String,
      allowedValues: ['active', 'completed'],
      optional: true,
      defaultValue: 'active',
    },
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      optional: true,
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      optional: true,
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

// Add custom validation
CampaignSchema.addValidator(function() {
  if (!this.obj) return false;
  if (this.isInsert) {
    if (this.obj.type === 'planned') {
      if (!this.obj.executionDate)
        return {
          name: 'executionDate',
          type: SimpleSchema.ErrorTypes.REQUIRED,
          value: "La date d'envoi est obligatoire",
        };
    } else {
      if (!this.obj.dateRef)
        return {
          name: 'dateRef',
          type: SimpleSchema.ErrorTypes.REQUIRED,
          value: 'La date de référence est obligatoire',
        };
    }
  }
  return true;
});

Campaigns.attachSchema(CampaignSchema);

export default Campaigns;
