import SimpleSchema from 'simpl-schema';
import { Tracker } from 'meteor/tracker';
import slugify from 'slugify';

SimpleSchema.extendOptions(['autoform']);

const Shops = new Mongo.Collection('shops');

Shops.deny({
  insert() { return true; },
  update() { return true; },
  remove() { return true; },
});

const TwilioConfigSchema = new SimpleSchema({
  senderId: String,
  sid: String,
  authToken: String,
});

const PackSchema = new SimpleSchema({
  name: String,
  expirationDate: Date,
  sms: Number,
});

const CreditSchema = new SimpleSchema({
  sms: {
    type: Number,
    defaultValue: 0,
  },
});

// Define the schema
export const ShopSchema = new SimpleSchema(
  {
    name: {
      type: String,
      label: 'Nom du magasin',
    },
    description: {
      type: String,
      label: 'Description',
      optional: true,
      autoform: {
        rows: 10,
      },
    },
    ownerId: {
      type: String,
      optional: true,
      autoform: {
        omit: true,
      },
    },
    state: {
      type: String,
      allowedValues: ['ok', 'expired', 'out', 'trial'],
      defaultValue: 'trial',
    },
    twilioConfig: {
      type: TwilioConfigSchema,
      optional: true,
    },
    currentPack: {
      type: PackSchema,
      optional: true,
    },
    credits: CreditSchema,
    createdAt: {
      type: Date,
      label: 'Date de création',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
    updatedAt: {
      type: Date,
      label: 'Date de modification',
      autoValue() {
        return new Date();
      },
      autoform: {
        omit: true,
      },
    },
  },
  { tracker: Tracker },
);

Shops.attachSchema(ShopSchema);

export default Shops;
