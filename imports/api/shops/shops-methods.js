import Campaigns from '../campaigns/campaigns-collection';
import Shops from './shops-collection';
import { SimpleSchema } from 'simpl-schema/dist/SimpleSchema';
import TwilioApi from '../twilio/twilio-api';

const editShopAdmin = new ValidatedMethod({
  name: 'editShopAdmin',
  validate: new SimpleSchema({
    shopId: String,
    senderName: String,
    remainingCredit: Number,
  }).validator(),
  run({ shopId, senderName, remainingCredit }) {
    const loggedInUser = Meteor.user();
    if (!loggedInUser || !Roles.userIsInRole(loggedInUser, ['admin'])) {
      throw new Meteor.Error(403, 'Access denied');
    }

    Shops.update(
      { _id: shopId },
      {
        $set: {
          'credits.sms': remainingCredit,
          'twilioConfig.senderId': senderName,
        },
      },
    );
  },
});
