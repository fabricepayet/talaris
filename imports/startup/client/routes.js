import { Meteor } from 'meteor/meteor';

function checkLoggedIn(ctx, redirect) {
  if (!Meteor.userId()) {
    redirect('/login');
  }
}

function checkAdmin(ctx, redirect) {
  if (!Roles.userIsInRole(Meteor.userId(), ['admin'])) {
    redirect('/login');
  }
}

function redirectIfLoggedIn(ctx, redirect) {
  if (Meteor.userId()) {
    redirect('/dashboard');
  }
}

FlowRouter.route('/login', {
  name: 'login',
  action: function(params, queryParams) {
    BlazeLayout.render('Layout_Noauth', { main: 'Login_Page' });
  },
});

// Tempory desactived for the beta

// FlowRouter.route('/register', {
//   name: 'register',
//   action: function(params, queryParams) {
//     BlazeLayout.render('Layout_Noauth', { main: 'Register_Page' });
//   },
// });

FlowRouter.route('/verify-email/:token', {
  name: 'verifyEmail',
  action(params) {
    Accounts.verifyEmail(params.token, err => {
      if (err) {
        toastr.error(err.reason);
      } else {
        toastr.success(
          'Votre email a bien été vérifié! Vous êtes maintenant connecté.',
        );
      }
      FlowRouter.go('/');
    });
  },
});

FlowRouter.route('/logout', {
  name: 'logout',
  action(params) {
    Meteor.logout(err => {
      FlowRouter.go('/');
    });
  },
});

FlowRouter.route('/reset-password/:token', {
  name: 'resetPassword',
  action(params) {
    BlazeLayout.render('Layout_Noauth', { main: 'Reset_Password_Page' });
  },
});

FlowRouter.route('/enroll-account/:token', {
  name: 'enrollAccount',
  action(params) {
    BlazeLayout.render('Layout_Noauth', { main: 'Enroll_Account_Page' });
  },
});

FlowRouter.route('/', {
  name: 'landing',
  triggersEnter: [redirectIfLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout_Noauth', { main: 'Landing_Page' });
  },
});

FlowRouter.route('/dashboard', {
  name: 'dashboard',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Dashboard_Page' });
  },
});

FlowRouter.route('/reports', {
  name: 'reports',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Reports_Page' });
  },
});

FlowRouter.route('/segments', {
  name: 'segments',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Segments_Page' });
  },
});

FlowRouter.route('/segments/detail/:segmentId', {
  name: 'segmentDetail',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Segment_Detail_Page' });
  },
});

FlowRouter.route('/segments/add', {
  name: 'AddSegment',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Add_Segment_Page' });
  },
});

FlowRouter.route('/products', {
  name: 'products',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Products_Page' });
  },
});

FlowRouter.route('/campaigns', {
  name: 'campaigns',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Campaigns_Page' });
  },
});

FlowRouter.route('/contacts', {
  name: 'contacts',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Contacts_Page' });
  },
});

FlowRouter.route('/lists', {
  name: 'lists',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Lists_Page' });
  },
});

FlowRouter.route('/contacts/add', {
  name: 'addContact',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Add_Contact_Page' });
  },
});

FlowRouter.route('/contacts/detail/:contactId', {
  name: 'contactDetail',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Contact_Detail_Page' });
  },
});

FlowRouter.route('/dashboard/update-shop', {
  name: 'updateShop',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Update_Shop_Page' });
  },
});

FlowRouter.route('/configuration', {
  name: 'configuration',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Configuration_Page' });
  },
});

FlowRouter.route('/lists/detail/:listId', {
  name: 'listDetail',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'List_Detail_Page' });
  },
});

FlowRouter.route('/invoices', {
  name: 'invoices',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Invoices_Page' });
  },
});

FlowRouter.route('/account', {
  name: 'account',
  triggersEnter: [checkLoggedIn],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Account_Page' });
  },
});

// ADMINISTRATION

FlowRouter.route('/administration', {
  name: 'administration',
  triggersEnter: [checkLoggedIn, checkAdmin],
  action: function(params, queryParams) {
    BlazeLayout.render('Layout', { main: 'Administration_Page' });
  },
});

// PUBLIC ROUTES

FlowRouter.route('/shop/:shopSlug', {
  name: 'publicShop',
  action: function(params, queryParams) {
    BlazeLayout.render('Public_Layout', { main: 'Public_Shop_Page' });
  },
});

FlowRouter.route('/legal/user-agreement', {
  name: 'userAgreement',
  action: function(params, queryParams) {
    BlazeLayout.render('Layout_Noauth', { main: 'User_Agreement_Page' });
  },
});

FlowRouter.route('/legal/privacy-policy', {
  name: 'privacyPolicy',
  action: function(params, queryParams) {
    BlazeLayout.render('Layout_Noauth', { main: 'Privacy_Policy_Page' });
  },
});

FlowRouter.route('/legal/cookie-policy', {
  name: 'cookiePolicy',
  action: function(params, queryParams) {
    BlazeLayout.render('Layout_Noauth', { main: 'Cookie_Policy_Page' });
  },
});

FlowRouter.notFound = {
  action: function() {
    console.error('route not found');
    FlowRouter.go('/');
  },
};

FlowRouter.wait();
Tracker.autorun(c => {
  if (Roles.subscription.ready() && !FlowRouter._initialized) {
    FlowRouter.initialize();
    c.stop();
  }
});
