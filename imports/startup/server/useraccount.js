import shopApi from '../../api/shops/shop-api';

shouldVerifyEmail = true;

Accounts.validateLoginAttempt(({ user }) => {
  // // reject users with role "blocked"
  // if (user && Roles.userIsInRole(user._id, ['blocked'])) {
  //   throw new Meteor.Error(403, 'Your account is blocked.');
  // }

  if (
    shouldVerifyEmail &&
    user &&
    user.emails &&
    user.emails.length &&
    !user.emails[0].verified
  ) {
    throw new Meteor.Error(499, 'E-mail not verified.');
  }

  return true;
});

Accounts.onCreateUser((options, user) => {
  if (user._id) {
    if (options.shopName) {
      shopApi.createNewShop(options.shopName, user._id);
      user.shop = options.shopName;
    } else {
      console.info('creating user without shop');
    }
  } else {
    console.error('Unknown error when creating new user');
    return;
  }
  return user;
});

Accounts.config({
  sendVerificationEmail: true,
});

Accounts.urls.verifyEmail = function(token) {
  return Meteor.absoluteUrl('verify-email/' + token);
};

Accounts.urls.resetPassword = function(token) {
  return Meteor.absoluteUrl('reset-password/' + token);
};

Accounts.urls.enrollAccount = function(token) {
  return Meteor.absoluteUrl('enroll-account/' + token);
};

Accounts.emailTemplates.siteName = 'Talaris';
Accounts.emailTemplates.from = 'Talaris <noreply@talaris.io>';

Accounts.emailTemplates.enrollAccount = {
  subject() {
    return `Bienvenue sur Talaris`;
  },

  text(user, url) {
    return (
      "Félicitation, vous avez maintenant un compte sur Talaris. Pour l'activer, cliquez simplement sur le lien ci-dessous:\n\n" +
      url
    );
  },
};

Accounts.emailTemplates.resetPassword = {
  subject() {
    return 'Changer votre mot de passe';
  },

  text(user, url) {
    return `Pour changer votre mot de passe sur Talaris, cliquez sur le lien ci-dessous:\n\n ${url}`;
  },
};

Accounts.emailTemplates.verifyEmail = {
  subject() {
    return 'Finaliser votre inscription';
  },
  text(user, url) {
    return `Pour terminer votre inscription sur Talaris, cliquez sur ce lien:\n\n ${url}`;
  },
};

// Deny all client-side updates to user documents
Meteor.users.deny({
  update() { return true; }
});