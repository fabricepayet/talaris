import Contacts from '../../api/contacts/contacts-collection';
import Shops from '../../api/shops/shops-collection';

// Contacts.permit('insert').ifHasRole('admin');

// Security.defineMethod('ownsDocument', {
//   fetch: [],
//   allow(type, field, userId, doc) {
//     console.log('type', type);
//     console.log('field', field);
//     console.log('userId', userId);
//     console.log('doc', doc);
//     if (!field) field = 'userId';
//     return userId === doc[field];
//   },
// });

// Shops.permit('update').ownsDocument('ownerId');

Shops.permit(['insert', 'update', 'remove']);
