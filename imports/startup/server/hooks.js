import Contacts from '../../api/contacts/contacts-collection';
import Lists from '../../api/lists/lists-collection';
import Checkouts from '../../api/checkouts/checkouts-collection';

// increment list count when creating a new contact
Contacts.after.insert(function(userId, doc) {
  Lists.update(
    { _id: { $in: doc.lists } },
    {
      $inc: { count: 1 },
    },
    { multi: true },
  );
});

// decrement list count when removing a contact
Contacts.after.remove(function(userId, doc) {
  Lists.update(
    { _id: { $in: doc.lists } },
    {
      $inc: { count: -1 },
    },
    { multi: true },
  );
});

Contacts.after.update(function(userId, doc, fieldNames, modifier, options) {
  if (doc.lists === this.previous.lists) {
    return;
  }

  const listsAdded = _.difference(doc.lists, this.previous.lists);
  const listsRemoved = _.difference(this.previous.lists, doc.lists);

  Lists.update(
    { _id: { $in: listsAdded } },
    {
      $inc: { count: 1 },
    },
    { multi: true },
  );

  Lists.update(
    { _id: { $in: listsRemoved } },
    {
      $inc: { count: -1 },
    },
    { multi: true },
  );
});

// updating lastCheckout for a contact
Checkouts.after.insert(function(userId, doc) {
  const contactId = doc.contactId;
  Contacts.update(
    { _id: contactId },
    {
      $set: {
        lastCheckoutDate: doc.date,
      },
    },
  );
});

// cleaning contact lists when removing a list
Lists.after.remove(function(userId, doc) {
  const contacts = Contacts.find({ lists: doc._id }).fetch();
  contacts.forEach(function(contact) {
    const lists = contact.lists;
    const position = lists.indexOf(doc._id);
    if (position > -1) {
      const newList = lists.splice(position, 1);
      Contacts.update(contact._id, { $set: { lists } });
    }
  });
});
