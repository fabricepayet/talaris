if (Meteor.users.find().count() === 0) {
  Meteor.settings.adminAccounts.forEach(function(email) {
    var userId = Accounts.createUser({ email });
    Roles.addUsersToRoles(userId, 'admin');
    Accounts.sendEnrollmentEmail(userId);
  });
}
